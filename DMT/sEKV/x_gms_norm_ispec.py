""" Rigorous implementation of the HICUM transfer current function from VAE

* all parameters

Possible input:

* gummel at VBC at T

Author: Markus Mueller | Markus.Mueller3@tu-dresden.de
"""

# DMT
# Copyright (C) from 2022  SemiMod
# Copyright (C) until 2021  Markus Müller, Mario Krattenmacher and Pascal Kuthe
# <https://gitlab.com/dmt-development/dmt-device>
#
# This file is part of DMT_other.
#
# DMT_other is free software for non-commercial use only. DMT_other is licensed
# under the DMT License.
#
# DMT_other is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# DMT_LICENSE.md in the root directory of DMT for more details.
#
# You should have received a copy of the DMT License along with this program.

import numpy as np
from DMT.core import specifiers, sub_specifiers, set_col_name, Plot, constants
from DMT.sEKV import McSekv

from DMT.extraction import (
    XStep,
    plot,
    print_to_documentation,
    XBounds,
    XYBounds,
    YBounds,
)

try:
    from DMT.external.pylatex import Tex
    from pylatex import NoEscape
except ImportError:
    pass


class XGmsNormIspec(XStep):
    """


    Parameters
    ----------
    name            : str
        Name of this specific xcjei object.
    mcard           : :class:`~DMT.core.mcard.MCard` or :class:`~DMT.core.mc_parameter.McParameterCollection`
        This MCard needs to hold all relevant parameters of the model and is used for simulations or model equation calculations.
    lib                     : :class:`~DMT.core.dut_lib.DutLib`
        This library of devices need to hold a relevant internal dut with data in one or more DataFrames as fitting reference.
    op_definition           : {key : float, tuple or list}
        Defines how to filter the given data in the duts by setting either a single value (float), a range (2-element tuple) or a list of values (list) for each variable to filter.

    model                   : VAE model
        VAE model with itf extraction
    normalized_transfer_current                   : bool
        Whether to normalize the measurements and model with `normalized_it`. Note that this normalization depends on the parameter mcf.
        However upon changing this data the measurements will NOT be prenormalized. Therefore this parameter should not be changed.
        If you want to optimize mcf please disable this flag
    area_densities                   : bool
        If True, the current density is normalized to the area of the device. If the keyword argument "normalized_transfer_current" is True, this argument is ignored.

    """

    def __init__(
        self,
        name,
        mcard,
        lib,
        op_definition,
        model=None,
        to_optimize=None,
        bounds_class=None,
        sort_by=None,
        **kwargs,
    ):
        if model is None:
            model = mcard.get_verilogae_model()
        self.model_function = self.fit
        if isinstance(mcard, McSekv):
            self.model_function_info = {
                "depends": (model.functions["i"],),
                "independent_vars": ("vgs", "t_dev"),
            }
        else:  # hicum
            self.model_function_info = {
                "depends": (model.functions["Id"],),
                "independent_vars": (
                    "vgs",
                    "vds",
                    "t_dev",
                ),
            }

        if bounds_class is None:
            bounds_class = XBounds

        if sort_by is None:
            sort_by = "x"

        if to_optimize is None:
            # if not given make a reasonable guess
            to_optimize = ["n", "ispec"]

        # init the super class
        super().__init__(
            name,
            mcard,
            lib,
            op_definition,
            model=model,
            to_optimize=to_optimize,
            specifier_paras={
                "specifier_outer_sweep": specifiers.VOLTAGE + ["D", "S"],
            },
            sort_by=sort_by,
            bounds_class=bounds_class,
            **kwargs,
        )

        self.col_vgs = set_col_name(specifiers.VOLTAGE, "G", "S")
        self.col_id = set_col_name(specifiers.CURRENT, "D")

        self.col_outer_sweep = self.specifier_paras["specifier_outer_sweep"]

    @plot()
    @print_to_documentation()
    def main_plot(self):
        main_plot = super(XGmsNormIspec, self).main_plot(
            r"$ I_{\mathrm{D}}/\left( g_{\mathrm{m}} V_{\mathrm{T}} \right)$",
            x_label=r"$ I_{\mathrm{D}} \left( \si{\milli\ampere} \right)$",
            y_label=r"$ I_{\mathrm{D}}/\left( g_{\mathrm{m}} V_{\mathrm{T}} \right)$",
            legend_location="upper left",
            x_log=True,
            y_log=True,
            x_scale=1e3,
        )
        return main_plot

    def get_tex(self):
        """Return a tex Representation of the Model that is beeing fitted. This can then be displayed in the UI."""
        return r"I_{\mathrm{D}} = f \left( V_{\mathrm{GS}} \right) "

    def get_description(self):
        doc = Tex()
        doc.append(
            NoEscape(
                r"This extraction step implements a rigorous fit of the sEKV transfer current model to measurement data."
            )
        )
        return doc

    def ensure_input_correct_per_dataframe(self, dataframe, **_kwargs):
        """Search for all required columns in the data frames."""
        dataframe.ensure_specifier_column(self.col_vgs)
        dataframe.ensure_specifier_column(self.col_outer_sweep)
        dataframe.ensure_specifier_column(self.col_id)

    def set_initial_guess(self, data_reference):
        pass  # at this point in the extraction we should already have good parameters.

    def init_data_reference_per_dataframe(self, dataframe, t_meas, **_kwargs):
        """Find the required data in the user supplied dataframe or database and write them into data_model attribute of XStep object."""
        for _index, v_a, data in dataframe.iter_unique_col(
            self.col_outer_sweep, decimals=2
        ):
            vgs = data[self.col_vgs].to_numpy()
            idrain = data[self.col_id].to_numpy()
            vt = constants.calc_VT(t_meas)

            try:
                temp = data[specifiers.TEMPERATURE].to_numpy()
            except KeyError:
                temp = t_meas

            line = {
                "x": np.abs(idrain),
                "y": idrain / np.gradient(idrain, vgs) / vt,
                self.col_id: idrain,
                self.col_vgs: vgs,
                specifiers.TEMPERATURE: temp,
            }
            self.data_reference.append(line)
            self.labels.append(
                r"$T=\SI{{{:.1f}}}{{\kelvin}}, \,{:s}=\SI{{{:.2f}}}{{\volt}}$".format(
                    t_meas,
                    (
                        specifiers.VOLTAGE + self.col_outer_sweep.nodes
                    ).to_tex(),  # skip the sub specifier
                    v_a,
                )
            )

    def fit(self, line, paras_model):
        kwargs = paras_model.to_kwargs()
        if isinstance(self.mcard, McSekv):
            line["y"] = (
                line["x"]
                / paras_model["ispec"].value
                * paras_model["n"].value
                * paras_model["lambdac"].value
            )
        else:
            raise IOError

        return line
