""" Rigorous implementation of the HICUM transfer current function from VAE

* all parameters

Possible input:

* gummel at VBC at T

Author: Markus Mueller | Markus.Mueller3@tu-dresden.de
"""

# DMT
# Copyright (C) from 2022  SemiMod
# Copyright (C) until 2021  Markus Müller, Mario Krattenmacher and Pascal Kuthe
# <https://gitlab.com/dmt-development/dmt-device>
#
# This file is part of DMT_other.
#
# DMT_other is free software for non-commercial use only. DMT_other is licensed
# under the DMT License.
#
# DMT_other is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# DMT_LICENSE.md in the root directory of DMT for more details.
#
# You should have received a copy of the DMT License along with this program.

import numpy as np
from DMT.core import specifiers, sub_specifiers, set_col_name, Plot
from DMT.sEKV import McSekv

from DMT.extraction import (
    XStep,
    plot,
    print_to_documentation,
    IYNormLog_1,
    XBounds,
    XYBounds,
    YBounds,
)

try:
    from DMT.external.pylatex import Tex
    from pylatex import NoEscape
except ImportError:
    pass


class XIds(XStep):
    """


    Parameters
    ----------
    name            : str
        Name of this specific xcjei object.
    mcard           : :class:`~DMT.core.mcard.MCard` or :class:`~DMT.core.mc_parameter.McParameterCollection`
        This MCard needs to hold all relevant parameters of the model and is used for simulations or model equation calculations.
    lib                     : :class:`~DMT.core.dut_lib.DutLib`
        This library of devices need to hold a relevant internal dut with data in one or more DataFrames as fitting reference.
    op_definition           : {key : float, tuple or list}
        Defines how to filter the given data in the duts by setting either a single value (float), a range (2-element tuple) or a list of values (list) for each variable to filter.

    model                   : VAE model
        VAE model with itf extraction
    normalized_transfer_current                   : bool
        Whether to normalize the measurements and model with `normalized_it`. Note that this normalization depends on the parameter mcf.
        However upon changing this data the measurements will NOT be prenormalized. Therefore this parameter should not be changed.
        If you want to optimize mcf please disable this flag
    area_densities                   : bool
        If True, the current density is normalized to the area of the device. If the keyword argument "normalized_transfer_current" is True, this argument is ignored.

    """

    def __init__(
        self,
        name,
        mcard,
        lib,
        op_definition,
        model=None,
        to_optimize=None,
        bounds_class=None,
        sort_by=None,
        **kwargs,
    ):
        if model is None:
            model = mcard.get_verilogae_model()
        self.model_function = self.fit
        if isinstance(mcard, McSekv):
            self.model_function_info = {
                "depends": (model.functions["i"],),
                "independent_vars": ("vgs", "t_dev"),
            }
        else:  # hicum
            self.model_function_info = {
                "depends": (model.functions["Id"],),
                "independent_vars": (
                    "vgs",
                    "vds",
                ),
            }

        if bounds_class is None:
            bounds_class = XBounds

        if sort_by is None:
            sort_by = "x"

        if to_optimize is None:
            # if not given make a reasonable guess
            to_optimize = ["n", "ispec"]

        # init the super class
        super().__init__(
            name,
            mcard,
            lib,
            op_definition,
            model=model,
            to_optimize=to_optimize,
            specifier_paras={
                "specifier_outer_sweep": specifiers.VOLTAGE + ["D", "S"],
            },
            sort_by=sort_by,
            bounds_class=bounds_class,
            **kwargs,
        )

        self.col_vgs = set_col_name(specifiers.VOLTAGE, "G", "S")
        self.col_vds = set_col_name(specifiers.VOLTAGE, "D", "S")
        self.col_id = set_col_name(specifiers.CURRENT, "D")

        self.col_outer_sweep = self.specifier_paras["specifier_outer_sweep"]
        self.iynorm = IYNormLog_1

    @plot()
    @print_to_documentation()
    def main_plot(self):
        main_plot = super(XIds, self).main_plot(
            r"$ I_{\mathrm{D}}$",
            x_specifier=self.col_vgs,
            y_specifier=self.col_id,
            legend_location="upper left",
            y_log=True,
        )
        return main_plot

    def get_tex(self):
        """Return a tex Representation of the Model that is beeing fitted. This can then be displayed in the UI."""
        return r"I_{\mathrm{D}} = f \left( V_{\mathrm{GS}} \right) "

    def get_description(self):
        doc = Tex()
        doc.append(
            NoEscape(
                r"This extraction step implements a rigorous fit of the sEKV transfer current model to measurement data."
            )
        )
        return doc

    def ensure_input_correct_per_dataframe(self, dataframe, **_kwargs):
        """Search for all required columns in the data frames."""
        dataframe.ensure_specifier_column(self.col_vgs)
        dataframe.ensure_specifier_column(self.col_outer_sweep)
        dataframe.ensure_specifier_column(self.col_id)

    def set_initial_guess(self, data_reference):
        pass  # at this point in the extraction we should already have good parameters.

    def init_data_reference_per_dataframe(self, dataframe, t_meas, **_kwargs):
        """Find the required data in the user supplied dataframe or database and write them into data_model attribute of XStep object."""
        for _index, v_a, data in dataframe.iter_unique_col(
            self.col_outer_sweep, decimals=2
        ):
            vgs = data[self.col_vgs].to_numpy()
            vds = data[self.col_vds].to_numpy()
            idrain = data[self.col_id].to_numpy()

            try:
                temp = data[specifiers.TEMPERATURE].to_numpy()
            except KeyError:
                temp = t_meas

            line = {
                "x": vgs,
                "y": idrain,
                self.col_id: idrain,
                self.col_vgs: vgs,
                "vgs": vgs,
                "vds": vds,
                specifiers.TEMPERATURE: temp,
            }
            self.data_reference.append(line)
            self.labels.append(
                r"$T=\SI{{{:.1f}}}{{\kelvin}}, \,{:s}=\SI{{{:.2f}}}{{\volt}}$".format(
                    t_meas,
                    (
                        specifiers.VOLTAGE + self.col_outer_sweep.nodes
                    ).to_tex(),  # skip the sub specifier
                    v_a,
                )
            )

    def fit(self, line, paras_model):
        kwargs = paras_model.to_kwargs()
        if isinstance(self.mcard, McSekv):
            idd = self.model.functions["i"].eval(
                temperature=line[specifiers.TEMPERATURE],
                voltages={"br_gs": line["x"], "br_ds": 0, "br_sb": 0},
                **kwargs,
            )
        else:
            idd = self.model.functions["Id"].eval(
                temperature=line[specifiers.TEMPERATURE],
                voltages={
                    "br_gs": line["vgs"],
                    "br_ds": line["vds"],
                    "br_gb": line["vgs"],
                    "br_sb": 0,
                    "br_db": line["vds"],
                },
                **kwargs,
            )

        line["y"] = idd
        line[self.col_id] = idd

        return line

    @plot()
    def ic_no_norm(self):
        plt = Plot(
            "id_norm",
            num=self.name + "id_norm",
            x_specifier=self.col_vgs,
            y_label=r"$I_{\mathrm{D}}/I_{\mathrm{Spec}}$",
            y_log=True,
            style="xtraction_color",
            legend_location="upper left",
        )
        for line_ref, line_model, label in zip(
            self.data_reference, self.data_model, self.labels
        ):
            plt.add_data_set(
                line_ref["x"],
                line_ref[self.col_id] / self.mcard["ispec"].value,
                label=label,
            )
            plt.add_data_set(
                line_model["x"], line_model[self.col_id] / self.mcard["ispec"].value
            )

        return plt
