""" Defines some HICUM L2 default circuits.




"""
# DMT
# Copyright (C) from 2022  SemiMod
# Copyright (C) until 2021  Markus Müller, Mario Krattenmacher and Pascal Kuthe
# <https://gitlab.com/dmt-development/dmt-device>
#
# This file is part of DMT_other.
#
# DMT_other is free software for non-commercial use only. DMT_other is licensed
# under the DMT License.
#
# DMT_other is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# DMT_LICENSE.md in the root directory of DMT for more details.
#
# You should have received a copy of the DMT License along with this program.

from DMT.core import Circuit, CircuitElement
from DMT.core.circuit import RESISTANCE, VOLTAGE, SHORT, CURRENT, CAPACITANCE


def get_circuit(circuit_type, modelcard):
    """
    Currently implemented:

    * 'common_source' : Common emitter configuration of a BJT with resistors that represents the parasitic connections to the transistor.


    Parameter
    ------------
    circuit_type : str
        For allowed types, see above
    modelcard : :class:`~DMT.core.mcard.MCard`

    Returns
    -----------
    circuit : :class:`~DMT.core.circuit.Circuit`

    """
    nodes = [f"n_{node.upper()}" for node in modelcard.nodes_list]
    node_drain = nodes[0]
    node_gate = nodes[1]
    node_source = nodes[2]
    node_bulk = nodes[3]

    circuit_elements = []

    # model instance
    circuit_elements.append(
        CircuitElement(
            modelcard.default_module_name,
            modelcard.default_subckt_name,
            nodes,
            parameters=modelcard,
        )
    )

    if circuit_type == "common_source":
        # GATE NODE CONNECTION #############
        # short for current measurement
        circuit_elements.append(
            CircuitElement(
                SHORT,
                "I_G",
                ["n_GX", "n_G_FORCED"],
            )
        )
        # metal resistance between contact gate point and real gate
        try:
            rgm = modelcard.get("_rgm").value
        except KeyError:
            rgm = 1e-3
        circuit_elements.append(
            CircuitElement(
                RESISTANCE,
                "Rgm",
                ["n_G_FORCED", node_gate],
                parameters=[("R", str(rgm))],
            )
        )
        # capacitance since AC already deembeded Rgm
        circuit_elements.append(
            CircuitElement(
                CAPACITANCE,
                "Cgm",
                ["n_G_FORCED", node_gate],
                parameters=[("C", str(1))],
            )
        )

        # DRAIN NODE CONNECTION #############
        # short for current measurement
        circuit_elements.append(
            CircuitElement(
                SHORT,
                "I_D",
                ["n_DX", "n_D_FORCED"],
            )
        )
        # metal resistance between contact drain point and real drain
        try:
            rdm = modelcard.get("_rdm").value
        except KeyError:
            rdm = 1e-3
        circuit_elements.append(
            CircuitElement(
                RESISTANCE,
                "Rdm",
                ["n_D_FORCED", node_drain],
                parameters=[("R", str(rdm))],
            )
        )
        # capacitance since AC already deembeded Rcm
        circuit_elements.append(
            CircuitElement(
                CAPACITANCE,
                "Cdm",
                ["n_D_FORCED", node_drain],
                parameters=[("C", str(1))],
            )
        )

        # SOURCE NODE CONNECTION #############
        # short for current measurement
        circuit_elements.append(
            CircuitElement(
                SHORT,
                "I_S",
                ["n_SX", "n_S_FORCED"],
            )
        )
        # metal resistance between contact source point and real source
        try:
            rsm = modelcard.get("_rsm").value
        except KeyError:
            rsm = 1e-3
        circuit_elements.append(
            CircuitElement(
                RESISTANCE,
                "Rsm",
                ["n_S_FORCED", node_source],
                parameters=[("R", str(rsm))],
            )
        )
        # capacitance since AC already deembeded Rcm
        circuit_elements.append(
            CircuitElement(
                CAPACITANCE,
                "Csm",
                ["n_S_FORCED", node_source],
                parameters=[("C", str(1))],
            )
        )

        # BULK NODE CONNECTION #############
        # short for current measurement
        circuit_elements.append(
            CircuitElement(
                SHORT,
                "I_B",
                ["n_BX", "n_B_FORCED"],
            )
        )
        # metal resistance between contact bulk point and real bulk
        try:
            rbm = modelcard.get("_rbm").value
        except KeyError:
            rbm = 1e-3
        circuit_elements.append(
            CircuitElement(
                RESISTANCE,
                "Rbm",
                ["n_B_FORCED", node_bulk],
                parameters=[("R", str(rbm))],
            )
        )
        # capacitance since AC already deembeded Rcm
        circuit_elements.append(
            CircuitElement(
                CAPACITANCE,
                "Cbm",
                ["n_B_FORCED", node_bulk],
                parameters=[("C", str(1))],
            )
        )

        # add sources
        circuit_elements.append(
            CircuitElement(
                VOLTAGE,
                "V_G",
                ["n_GX", "0"],
                parameters=[("Vdc", "V_G"), ("Vac", "V_G_ac")],
            )
        )
        circuit_elements.append(
            CircuitElement(
                VOLTAGE,
                "V_D",
                ["n_DX", "0"],
                parameters=[("Vdc", "V_D"), ("Vac", "V_D_ac")],
            )
        )
        circuit_elements.append(
            CircuitElement(
                VOLTAGE,
                "V_S",
                ["n_SX", "0"],
                parameters=[("Vdc", "V_S"), ("Vac", "V_S_ac")],
            )
        )
        circuit_elements.append(
            CircuitElement(
                VOLTAGE,
                "V_B",
                ["n_BX", "0"],
                parameters=[("Vdc", "V_B"), ("Vac", "V_B_ac")],
            )
        )
        circuit_elements += [
            "V_G=0",
            "V_D=0",
            "V_S=0",
            "V_B=0",
            "ac_switch=0",
            "V_G_ac=1-ac_switch",
            "V_D_ac=ac_switch",
            "V_S_ac=0",
            "V_B_ac=0",
        ]
    else:
        raise IOError("The circuit type " + circuit_type + " is unknown!")

    return Circuit(circuit_elements)
