""" modelcard for sEKV 6 parameters
"""

# DMT sEKV
# Copyright (C) 2023  SemiMod
# <https://gitlab.com/dmt-development/dmt-sEKV>
from __future__ import annotations
import copy
import numpy as np

try:
    from semver.version import Version as VersionInfo
except ImportError:
    from semver import VersionInfo

from DMT.core import MCard
from DMT.core.circuit import Circuit
from DMT.sEKV import default_va_files, get_circuit

SEMVER_MCSEKV_CURRENT = VersionInfo(major=1, minor=0)


class McSekv(MCard):
    """All model parameters of sEKV

    Parameters
    ----------
    va_file : str, optional
        Path to a Verilog-AMS file
    load_model_from_path : str, optional
        Initialise the modelcard with the parameter from the given file path.
    version : float, optional
        Version of the model card. Default is 1.0
    """

    def __init__(
        self,
        load_model_from_path=None,
        version=1.0,
        default_circuit="common_source",
        __McSekv__=SEMVER_MCSEKV_CURRENT,
        nodes_list=("D", "G", "S", "B"),
        default_subckt_name="Q_SEKV",
        default_module_name="sEKV_va",
        possible_groups=None,
        vae_module=None,
        va_file=default_va_files.sEKV,
        **kwargs,
    ):
        if possible_groups is None:
            possible_groups = {}

        super().__init__(
            nodes_list,
            default_subckt_name,
            default_module_name,
            version=version,
            possible_groups=possible_groups,
            vae_module=vae_module,
            va_file=va_file,
            **kwargs,
        )

        if not isinstance(__McSekv__, VersionInfo):
            try:
                __McSekv__ = VersionInfo.parse(__McSekv__)
            except TypeError:
                __McSekv__ = VersionInfo.parse(f"{__McSekv__:1.1f}.0")

        if __McSekv__ != SEMVER_MCSEKV_CURRENT:
            raise IOError("DMT->McSekv: The given version of __McSekv__ is unknown!")

        if not self._va_codes:
            raise IOError("DMT->McSekv: Only work with given verilog-a code!")

        if load_model_from_path is not None:
            super().load_model_parameters(load_model_from_path, force=False)

        self.default_circuit = default_circuit

    def info_json(self, **kwargs):
        """Returns a dict with serializeable content for the json file to create. Add the info about the concrete subclass to create here!"""
        info_dict = super(McSekv, self).info_json(**kwargs)
        info_dict["__McSekv__"] = str(SEMVER_MCSEKV_CURRENT)
        info_dict["default_circuit"] = self.default_circuit

        return info_dict

    def get_build_in(self):
        """Return the parameters embedded in a build-in model (no Va code and correct module name etc)"""
        raise IOError(
            "DMT->sEKV: sEKV has no build in version in any simulator! Use Verilog-A code!"
        )

    def get_circuit(self, use_build_in=False, topology=None, **kwargs) -> Circuit:
        """Here the modelcard defines it's default simulation circuit.

        Parameters
        ----------
        use_build_in : {False, True}, optional
            Creates a circuit for the modelcard using the build-in model
        topology : optional
            If a model has multiple standard circuits, use the topology to differentiate between them..
        """
        if use_build_in:
            mcard = self.get_build_in()
        else:
            mcard = self

        if topology is None:
            topology = self.default_circuit

        return get_circuit(topology, mcard)

    def get_clean_modelcard(self):
        """Returns all parameters which are part of sEKV and adds correct units"""
        default_mcard = McSekv(
            version=self.version,
            va_codes=self.va_codes,
            default_circuit=self.default_circuit,
        )
        for para in self:
            if para in default_mcard:
                default_mcard.set(para)
            else:
                print(
                    "Warning: parameter "
                    + para.name
                    + " was removed by get_clean_modelcard."
                )

        return default_mcard

    def get_internal_modelcard(self):
        """Returns all parameters which are part of sEKV internal

        In fact returns all parameters but all external parameters are set to their default values.
        """
        raise NotImplementedError()
