# DMT sEKV
# Copyright (C) 2023  SemiMod
# <https://gitlab.com/dmt-development/dmt-sEKV>
name = "sEKV"

# some va codes in the package, this way they can be used easily:
from pathlib import Path

path_sEKV_module = Path(__file__).resolve().parent

VA_FILES = {
    "sEKV": path_sEKV_module / "sEKV.va",
    "EKV": path_sEKV_module / "ekv2p6.va"
}


class _DefaultVAFiles(object):
    def __init__(self):
        self.sEKV = VA_FILES["sEKV"]
        self.EKV = VA_FILES["EKV"]


default_va_files = _DefaultVAFiles()
from .sekv_default_circuits import get_circuit

# sEKV model card
from .mc_sekv import McSekv
from .mc_ekv import McEkv

# sEKV Xtraction steps
from .x_ids import XIds
from .x_gms_norm_n import XGmsNormN
from .x_gms_norm_ispec import XGmsNormIspec
from .x_gm_norm_full import XGmsNormFull
