""" Test va file here!"""
import verilogae
import numpy as np
from DMT.sEKV import default_va_files
from DMT.core import Plot


def test_load():
    model = verilogae.load(default_va_files.sEKV)

    assert model.module_name == "sEKV_va"
    assert model.nodes == ["g", "d", "s"]
    assert list(model.modelcard.keys()) == [
        "n",
        "ispec",
        "vt0",
        "lambdac",
    ]
    # assert model.op_vars == []
    # assert model.functions == []


def test_model():
    model = verilogae.load(default_va_files.sEKV)
    vgs   = np.linspace(0.1,1,101)
    i = model.functions["i"].eval(
        voltages={"br_gs": vgs, "br_ds": 0},
        temperature=300,
        n=1.08,
        ispec=912e-9,
        vt0=0.411,
        lambdac=0,
    )/912e-9

    assert np.all(i < 1e3)

    # vor visualization
    # plt_id_vg = Plot("I_D(V_G)", x_label=r"$V_{\mathrm{GS}} \left( \si{\volt} \right)$", y_label=r"$I_{\mathrm{D}}/I_{\mathrm{D,spec}}$", y_log=True)
    # plt_id_vg.add_data_set(vgs, i)
    # plt_id_vg.plot_py(show=True)

if __name__ == "__main__":
    test_load()
    test_model()
