""" test sEKV mcard
"""
import numpy as np
from pathlib import Path
from DMT.config import COMMANDS

COMMANDS["OPENVAF"] = "openvaf"
from DMT.core import DutType, Sweep, specifiers, SimCon, Plot
from DMT.core.sweep_def import SweepDefConst, SweepDefList, SweepDefLinear
from DMT.sEKV import McSekv
from DMT.ngspice import DutNgspice

path_curr = Path(__file__).parent

col_vg = specifiers.VOLTAGE + "G"
col_vd = specifiers.VOLTAGE + "D"
col_vs = specifiers.VOLTAGE + "S"
col_vgs = specifiers.VOLTAGE + ["G", "S"]
col_vds = specifiers.VOLTAGE + ["D", "S"]
col_ig = specifiers.CURRENT + "G"
col_id = specifiers.CURRENT + "D"
col_is = specifiers.CURRENT + "D"


def test_modelcard():
    mcard = McSekv()

    path_test_mc = path_curr / "test_mc.json"

    mcard.dump_json(path_test_mc)

    mcard2 = McSekv.load_json(path_test_mc)

    assert mcard == mcard2

    path_test_mc.unlink()


def create_dut_ngspice():
    mcard = McSekv()

    return DutNgspice(
        None,
        DutType.n_mos,
        input_circuit=mcard,
        reference_node="S",
        copy_va_files=False,
        simulator_command="ngspice_osdi",
    )


def create_sweep():
    # create a sweep
    sweepdef = [
        SweepDefLinear(col_vg, start=0, stop=1, steps=11, sweep_order=3),
        SweepDefList(col_vd, value_def=np.linspace(0.5, 4, 8), sweep_order=2),
        SweepDefConst(col_vs, value_def=0, sweep_order=1),
    ]
    outputdef = ["I_G", "I_D", "OpVar"]
    othervar = {"TEMP": 300}
    return Sweep("IDVG", sweepdef=sweepdef, outputdef=outputdef, othervar=othervar)


def test_simulation():
    dut = create_dut_ngspice()
    sweep = create_sweep()
    df_sweep = sweep.create_df()

    sim_con = SimCon(n_core=1)
    sim_con.append_simulation(dut=dut, sweep=sweep)
    sim_con.run_and_read(force=True, remove_simulations=False)

    df_sim = dut.get_data(sweep=sweep)
    v_g = np.real(df_sim[col_vg].to_numpy())
    v_d = np.real(df_sim[col_vd].to_numpy())
    v_s = np.real(df_sim[col_vs].to_numpy())
    i_g = np.real(df_sim[col_ig].to_numpy())
    i_d = np.real(df_sim[col_id].to_numpy())
    i_s = np.real(df_sim[col_is].to_numpy())

    assert np.allclose(v_g, df_sweep[col_vg])
    assert np.allclose(v_d, df_sweep[col_vd])
    assert np.allclose(v_s, df_sweep[col_vs])


if __name__ == "__main__":
    test_modelcard()
    test_simulation()

    if True:
        dut = create_dut_ngspice()
        sweep = create_sweep()

        sim_con = SimCon(n_core=1)
        sim_con.append_simulation(dut=dut, sweep=sweep)
        sim_con.run_and_read(force=True, remove_simulations=False)

        df_sim = dut.get_data(sweep=sweep)

        plt_gummel = Plot(
            r"$I_{\mathrm{D}}(V_{\mathrm{GS}})$",
            x_label=r"$V_{\mathrm{GS}}$",
            y_label=r"$I_{\mathrm{D}}$",
            y_log=True,
            style="bw",
        )
        plt_ig = Plot(
            r"$I_{\mathrm{G}}(V_{\mathrm{GS}})$",
            x_label=r"$V_{\mathrm{GS}}$",
            y_label=r"$I_{\mathrm{G}}$",
            y_log=True,
            style="bw",
        )

        df_sim.ensure_specifier_column(col_vgs, ports=dut.nodes)
        df_sim.ensure_specifier_column(col_vds, ports=dut.nodes)

        for _index, vds, data in df_sim.iter_unique_col(col_vds, decimals=3):
            plt_gummel.add_data_set(
                np.real(data[col_vgs].to_numpy()),
                np.real(data[col_id].to_numpy()),
                label=col_vds.to_legend_with_value(np.real(vds)),
            )
            plt_ig.add_data_set(
                np.real(data[col_vgs].to_numpy()),
                np.real(data[col_ig].to_numpy()),
                label=col_vds.to_legend_with_value(np.real(vds)),
            )

    plt_ig.plot_pyqtgraph(show=False)
    plt_gummel.plot_pyqtgraph(show=True)
