import setuptools
import numpy

setuptools.setup(
    name="DMT_sEKV",
    version="0.1.0",
    author="M.Mueller, M.Krattenmacher",
    author_email="markus.mueller@semimod.de, mario.krattenmacher@semimod.de",
    description="Device Modeling Toolkit sEKV submodule",
    # long_description=long_description,
    # long_description_content_type="text/markdown",
    url="https://gitlab.com/dmt-development/dmt_sEKV",
    # packages=setuptools.find_packages(),
    packages=setuptools.find_namespace_packages(include=["DMT.*"]),
    license="GNU GPLv3+",
    classifiers=[
        "Programming Language :: Python :: 3.8",
        "Operating System :: OS Independent",
        "License :: OSI Approved :: GNU Affero General Public License v3",
    ],
    include_dirs=[numpy.get_include()],
    install_requires=[
        #"DMT-core[HDF5,pyqtgraph,pyside2,latex] @ git+ssh://git@gitlab.com/dmt-development/dmt-core.git",
        #"DMT-extraction @ git+ssh://git@gitlab.com/dmt-development/dmt-extraction.git",
        "DMT-core",
        "DMT-extraction"
    ],
)
